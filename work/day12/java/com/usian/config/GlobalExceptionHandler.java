package com.usian.config;

import com.baomidou.mybatisplus.extension.api.R;
import com.usian.pojo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NameExistsException.class)
    public Result exceptionHandler(NameExistsException e){
        e.printStackTrace();
        return Result.error(e.getMessage()+"已经存在");
    }

    @ExceptionHandler(Exception.class)
    public Result exceptionHandler(Exception e){
        e.printStackTrace();
        return Result.error("目前访问人数过多，请稍后！");
    }


    @ExceptionHandler(CategoryException.class)
    public Result exceptionHandler(CategoryException e){
        e.printStackTrace();
        return Result.error(e.getMessage());
    }
}
