package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.Setmeal;

public interface SetMealMapper extends BaseMapper<Setmeal> {
}
