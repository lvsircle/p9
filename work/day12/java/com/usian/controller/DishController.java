package com.usian.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.DishDto;
import com.usian.pojo.Result;
import com.usian.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping
    public Result dish(@RequestBody DishDto dishDto , HttpSession session){
        Long employee = (Long) session.getAttribute("employee");
        dishDto.setCreateUser(employee);
        dishDto.setUpdateUser(employee);
        dishService.save(dishDto);
        return Result.success("添加成功");
    }

    @GetMapping("page")
    public Result page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pagesize , String name){
        Page<DishDto> dtoPage = dishService.findPage(page,pagesize,name);
        return Result.success(dtoPage);
    }

    @GetMapping("{id}")
    public Result findById(@PathVariable Long id){
        DishDto dishDto = dishService.findById(id);
        return Result.success(dishDto);
    }

    @PutMapping
    public Result updateDish(@RequestBody  DishDto dishDto,HttpSession session){
        //1. 补全创建人与修改人
        Long empId = (Long) session.getAttribute("employee");
        dishDto.setUpdateUser(empId);
        //2. 交给service
        dishService.updateDish(dishDto);
        return Result.success("修改菜品成功");
    }

}
