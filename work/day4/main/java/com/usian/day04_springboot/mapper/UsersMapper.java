package com.usian.day04_springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.day04_springboot.pojo.Users;

public interface UsersMapper extends BaseMapper<Users> {
}
