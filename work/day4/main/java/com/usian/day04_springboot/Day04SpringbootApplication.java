package com.usian.day04_springboot;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.usian.day04_springboot.mapper")
public class Day04SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day04SpringbootApplication.class, args);
    }

}

