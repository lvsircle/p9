package com.usian.day04_springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UsersController {

    @GetMapping("/hellow")

    public String hellow(Model model){
        model.addAttribute("msg","hellow thymeleafe");
        return "index";
    }

}
