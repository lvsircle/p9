package com.usian.day03springboot.pojo;

import lombok.Data;

@Data
public class Student {

    private Integer id;
    private String name;
    private Integer age;
    private String sex;
    private String hobby;

}
