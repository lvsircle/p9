package com.usian.day03springboot.mapper;

import com.usian.day03springboot.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper
public interface StudentMapper {


    public List<Student> findAll();


    Student stuId(Integer id);

    List<Student> stuname(String name);
}
