package com.usian.day03springboot.controller;

import com.usian.day03springboot.mapper.StudentMapper;
import com.usian.day03springboot.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentMapper studentMapper;

    @GetMapping("/stu")
    public List<Student> findAll(){
        List<Student> list = studentMapper.findAll();
        return list;
    }

    @GetMapping("/stuId")
    public Student stuId(Integer id){
        return  studentMapper.stuId(id);
    }


    @GetMapping("/stuname")
    public List<Student> stuname(String name){
        return  studentMapper.stuname(name);
    }
}
