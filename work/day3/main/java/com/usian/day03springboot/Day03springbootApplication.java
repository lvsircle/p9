package com.usian.day03springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day03springbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day03springbootApplication.class, args);
    }

}
