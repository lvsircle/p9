package com.usian.service;

import com.usian.pojo.Order;

public interface OrderService {
    Order findOrderByOrderId(Long orderId);
}
