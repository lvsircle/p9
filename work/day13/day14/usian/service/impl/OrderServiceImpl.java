package com.usian.service.impl;

import com.usian.mapper.OrderMapper;
import com.usian.pojo.Order;
import com.usian.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Order findOrderByOrderId(Long id) {
        return orderMapper.selectById(id);
    }
}
