package com.usian.pojo;

import com.usian.pojo.User;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tb_order")
public class Order {

    private Long id;
    @TableField(value = "user_id")
    private Integer userId;
    private String name;
    private Integer price;
    private Integer num;
    private User user;
}
