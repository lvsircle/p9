package com.usian.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tb_user")
public class User {

    private Long id;
    private String username;
    private String address;

}
