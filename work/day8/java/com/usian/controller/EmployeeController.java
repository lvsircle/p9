package com.usian.controller;

import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import com.usian.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;


@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/login")
    public Result login(@RequestBody Employee employee , HttpSession session){
        Result<Employee> result = employeeService.login(employee);
        if (result.getCode()==1) {
            session.setAttribute("user", result.getData().getId());
        }
        return result;
    }

    @PostMapping("/logout")
    public Result logout(HttpSession session){
        session.invalidate();
        return Result.success("退网了，不用想我");
    }

}
