package com.usian.service;

import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import org.springframework.stereotype.Service;


public interface EmployeeService {
    Result<Employee> login(Employee employee);
}
