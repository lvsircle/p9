package com.usian.service.impl;

import com.usian.mapper.EmployeeMapper;
import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import com.usian.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
@Service
public class EmployeeServiceimpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;


    @Override
    public Result<Employee> login(Employee employee) {

        Employee employee1 = employeeMapper.login(employee);
        if (employee1==null){
            return Result.error("用户名不存在");
        }
        String oldpassword = employee1.getPassword();
        String newpassword = employee.getPassword();
        String s = DigestUtils.md5DigestAsHex(newpassword.getBytes());
        if (!s.equals(oldpassword)){
            return Result.error("你猜猜哪个错了");
        }
        if (employee1.getStatus()==0){
            return Result.error("用户已被拉入黑名单");
        }
        return Result.success(employee1);
    }
}
