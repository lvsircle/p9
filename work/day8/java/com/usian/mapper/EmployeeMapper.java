package com.usian.mapper;

import com.usian.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/*@Mapper*//*
@Repository*/
@Mapper
public interface EmployeeMapper {
    @Select("select * from employee where username=#{username}")
    Employee login(Employee employee);
}
