package com.usian;



import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = {"com.usian.mapper"})
@SpringBootApplication
public class RuijiApplication {
    public static void main(String[] args) {
        SpringApplication.run(RuijiApplication.class);
    }
}
