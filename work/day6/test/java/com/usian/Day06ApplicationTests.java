package com.usian;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.mapper.UsersMapper;
import com.usian.pojo.Users;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class Day06ApplicationTests {

    @Autowired
    private UsersMapper usersMapper;


    @Test
    public void findAll(){
        List<Users> list = usersMapper.selectList(null);
        System.out.println(list);
    }

    @Test
    public void findById(){
        Users users = usersMapper.selectById(1);
        System.out.println(users);
    }

    @Test
    public void insert(){
        Users users = new Users();
        users.setId(6);
        users.setName("张三");
        users.setSex("男");
        users.setAge(26);
        usersMapper.insert(users);
    }

    @Test
    public void delete(){
        usersMapper.deleteById(6);
    }

    @Test
    public void update(){
        Users users = new Users();
        users.setName("张三");
        users.setSex("男");
        users.setAge(26);
        usersMapper.updateById(users);
    }

    @Test
    public void selectPage(){
        Page<Users> page = new Page<Users>(1,2);
        usersMapper.selectPage((IPage) page,null);
        System.out.println("当前页为："+page.getCurrent());
        System.out.println("每页显示为："+page.getSize());
        System.out.println("总数量为："+page.getTotal());
        System.out.println("总页数："+page.getPages());
        System.out.println("当前页数据为："+page.getRecords());
    }


    @Test
    public void deleteAll(){
        List list = new ArrayList();
        list.add(2);
        list.add(3);
        usersMapper.deleteBatchIds(list);
    }

}
