package com.usian.config;

public class CategoryException extends RuntimeException{
    public CategoryException(String message){
        super(message);
    }
}
