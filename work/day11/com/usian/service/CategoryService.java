package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Category;

import java.util.List;


public interface CategoryService {
    void add(Category category);

    Page<Category> findPage(Integer page, Integer pageSize);

    void delete(Long id);

    void update(Category category);

    List<Category> findAll();
}
