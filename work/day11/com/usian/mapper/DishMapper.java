package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.Dish;

public interface DishMapper extends BaseMapper<Dish> {
}
