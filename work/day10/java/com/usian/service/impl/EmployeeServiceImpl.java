package com.usian.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.config.NameExistsException;
import com.usian.mapper.EmployeeMapper;
import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import com.usian.service.EmployeeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;


@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    public EmployeeMapper employeeMapper;
    @Override
    public Result<Employee> login(Employee employee) {
        //根据用户查询是否存在
        Employee employee1=employeeMapper.login(employee);
        //判断用户是否存在
        if (employee1==null){
            return Result.error("用户不存在");
        }
        //获取当前密码和以前密码的加密是否相同
        String oldpssword=employee1.getPassword();
        String nowpssword=employee.getPassword();
        String s =DigestUtils.md5DigestAsHex(nowpssword.getBytes());
        if (!s.equals(oldpssword)){
            return Result.error("用户或者密码错误");
        }
        if (employee1.getStatus()==0){
            return Result.error("用户已经被禁用");
        }
        return Result.success(employee1);
    }

    @Override
    public void save(Employee employee) {

        Employee adEmp=employeeMapper.login(employee);
        if (adEmp!=null){
            throw  new NameExistsException(employee.getUsername());
        }
        //1.补全用户的数据
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //2. 补全创建时间与修改时间、状态
        employee.setStatus(1);
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employeeMapper.insert(employee);
    }

    @Override
    public Page<Employee> page(Integer page, Integer pageSize, String name) {
        Page<Employee> page1=new Page<>(page,pageSize);
        LambdaQueryWrapper<Employee> queryWrapper=new LambdaQueryWrapper<>();
        /*queryWrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);*/
        queryWrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);
        Page<Employee>page2=employeeMapper.selectPage(page1,queryWrapper);
        return page2;
    }

    @Override
    public void updateEmp(Employee employee) {
        employee.setUpdateTime(LocalDateTime.now());
        LambdaUpdateWrapper<Employee> updateWrapper=new LambdaUpdateWrapper<>();
        if(employee.getUsername()!=null && employee.getUsername()!=""){
            updateWrapper.set(Employee::getUsername,employee.getUsername());
        }
    /*   employee.setUpdateTime(LocalDateTime.now());
       LambdaQueryWrapper<Employee>updateWrapper =new LambdaQueryWrapper<>();
       if (employee.getUsername()!=null && employee.getUsername()!=""){
           updateWrapper.set(Employee::getUsername,employee.getUsername());
       }*/

        if(employee.getName()!=null && employee.getName()!=""){
            updateWrapper.set(Employee::getName,employee.getName());
        }
        if(employee.getPassword()!=null && employee.getPassword()!=""){
            updateWrapper.set(Employee::getPassword,employee.getPassword());
        }
        if(employee.getPhone()!=null && employee.getPhone()!=""){
            updateWrapper.set(Employee::getPhone,employee.getPhone());
        }
        if(employee.getSex()!=null && employee.getSex()!=""){
            updateWrapper.set(Employee::getSex,employee.getSex());
        }
        if(employee.getIdNumber()!=null && employee.getIdNumber()!=""){
            updateWrapper.set(Employee::getIdNumber,employee.getIdNumber());
        }
        if(employee.getStatus()!=null){
            updateWrapper.set(Employee::getStatus,employee.getStatus());
        }
        if(employee.getUpdateTime()!=null ){
            updateWrapper.set(Employee::getUpdateTime,employee.getUpdateTime());
        }
        if(employee.getUpdateUser()!=null ){
            updateWrapper.set(Employee::getUpdateUser,employee.getUpdateUser());
        }
        updateWrapper.eq(Employee::getId,employee.getId());
        employeeMapper.update(employee,updateWrapper);
    }

    @Override
    public Employee findById(Long id) {
        Employee employee=employeeMapper.selectById(id);
        return employee;
    }

}
