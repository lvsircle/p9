package com.usian.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Employee {
    private Long id;
    private String name;
    private String username;
    private String password;
    private String phone;
    private String sex;
    private String idNumber;
    private Integer status;

    private LocalDateTime createTime;  //创建用户的时间

    private LocalDateTime updateTime; //修改用户的时间

    private Long createUser;  //创建用户的人


    private Long updateUser; //修改用户的人
}