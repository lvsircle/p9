package com.usian.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Users;

import java.util.List;


public interface UsersMapper<BaseMapper> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<Users> {
    List<Users> selectList(Object o);

    Users selectById(int i);

    int insert(Users users);

    void deleteById(int i);

    int updateById(Users users);

    void selectPage(Page<Users> page, Object o);

    void deleteBatchIds(List list);
}
